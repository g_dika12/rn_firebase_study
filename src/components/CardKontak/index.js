import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const CardKontak = ({ id, kontakItem, navigation, removeData }) => {
    return (
        <View
            key={id}
            style={styles.container}>
            <TouchableOpacity
                onPress={() => { navigation.navigate('DetailKontak', { id: id }) }}
            >
                <View>
                    <Text style={styles.nama}>{kontakItem.nama}</Text>
                    <Text style={styles.noHP}>No. HP : {kontakItem.nomorHP}</Text>
                </View>
            </TouchableOpacity>
            <View style={styles.icon}>
                <FontAwesomeIcon
                    onPress={() => { navigation.navigate('EditKontak', { id: id }) }}
                    icon={faEdit} color='orange' size={25} />
                <FontAwesomeIcon icon={faTimes} color='red' size={25} onPress={() => removeData(id)} />
            </View>
        </View>
    )
}

export default CardKontak;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 15,
        backgroundColor: '#fff',
        borderRadius: 5,
        marginBottom: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    },
    nama: {
        fontWeight: 'bold',
        fontSize: 16
    },
    noHP: {
        color: 'gray',
        fontSize: 12
    },
    icon: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    }
})
