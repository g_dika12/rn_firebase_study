import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'

const InputData = ({
    label,
    placeholder,
    keyboardType,
    isTextArea,
    onChangeText,
    value,
    namaState
}) => {
    if (isTextArea) {
        return (
            <>
                <Text style={styles.label}>{label} : </Text>
                <TextInput
                    multiline={true}
                    numberOfLines={4}
                    keyboardType={keyboardType}
                    placeholder={placeholder} style={styles.textInputArea}
                    value={value}
                    onChangeText={(text) => onChangeText(namaState, text)}
                />
            </>
        )
    }
    return (
        <>
            <Text style={styles.label}>{label} : </Text>
            <TextInput
                keyboardType={keyboardType}
                placeholder={placeholder} style={styles.textInput}
                value={value}
                onChangeText={(text) => onChangeText(namaState, text)}
            />
        </>
    )
}

export default InputData

const styles = StyleSheet.create({
    label: {
        fontSize: 16,
        marginBottom: 5
    },
    textInput: {
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 8,
        padding: 10,
        marginBottom: 10
    },
    textInputArea: {
        textAlignVertical: 'top',
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 8,
        padding: 10,
        marginBottom: 10
    }
})
