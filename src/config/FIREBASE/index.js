import firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyCVLCgvtqjmpwX5rBcYdfD61RhxCvgVqk4",
    authDomain: "crud-rn-firebase-f6cec.firebaseapp.com",
    projectId: "crud-rn-firebase-f6cec",
    storageBucket: "crud-rn-firebase-f6cec.appspot.com",
    messagingSenderId: "387470827805",
    appId: "1:387470827805:web:cabc5cb822904312cf41f4"
})

const FIREBASE = firebase;

export default FIREBASE;