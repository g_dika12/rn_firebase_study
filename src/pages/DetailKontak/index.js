import React, { Component } from 'react'
import { Text, StyleSheet, View, Alert } from 'react-native'
import FIREBASE from '../../config/FIREBASE'

export default class DetailKontak extends Component {
    constructor(props) {
        super(props)

        this.state = {
            kontak: {}
        }
    }

    componentDidMount() {
        FIREBASE.database()
            .ref("Kontak/" + this.props.route.params.id)
            .once("value", (querySnapShot) => {
                let data = querySnapShot.val() ? querySnapShot.val() : {};
                let kontakItem = { ...data }
                this.setState({
                    kontak: kontakItem
                })
            })
    }

    

    render() {
        const { kontak } = this.state;
        return (
            <View style={styles.pages}>
                <Text>Name :  </Text>
                <Text style={styles.text}> {kontak.nama} </Text>
                <Text>Nomor HP :  </Text>
                <Text style={styles.text}> {kontak.nomorHP} </Text>
                <Text>Alamar:  </Text>
                <Text style={styles.text}> {kontak.alamat} </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    pages: {
        margin: 30,
        padding: 20,
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 6
    },
    text: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10
    }
})
