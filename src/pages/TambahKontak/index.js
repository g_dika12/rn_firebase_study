import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, View, Text, Alert } from 'react-native'
import { InputData } from '../../components'
import FIREBASE from '../../config/FIREBASE'

export default class TambahKontak extends Component {
    constructor(props) {
        super(props)

        this.state = {
            nama: "",
            nomorHP: "",
            alamat: ""
        }
    }

    onChangeText = (namaState, value) => {
        this.setState({
            [namaState]: value
        })
    }

    onSubmit = () => {
        // console.log(this.state);
        if (this.state.nama && this.state.nomorHP && this.state.alamat) {
            console.log(this.state)
            const kontakReferensi = FIREBASE.database().ref('Kontak');
            const kontak = {
                nama: this.state.nama,
                nomorHP: this.state.nomorHP,
                alamat: this.state.alamat
            }
            kontakReferensi
            .push(kontak)
            .then((data)=>{
                this.props.navigation.replace('Home')
            })
            .catch((error)=>{
                console.log(error);
            })
        } else {
            Alert.alert("Error", "Nama, no hp, dan alamat wajib diisi")
        }
    }
    render() {
        return (
            <View style={styles.pages}>
                <InputData
                    label="Nama"
                    placeholder="input nama"
                    onChangeText={this.onChangeText}
                    value={this.state.nama}
                    namaState="nama"
                />
                <InputData
                    label="No Hp"
                    placeholder="masukkan no Hp" keyboardType='number-pad'
                    onChangeText={this.onChangeText}
                    value={this.state.nomorHP}
                    namaState="nomorHP"
                />
                <InputData
                    label="Alamat"
                    placeholder="Masukkan Alamat"
                    onChangeText={this.onChangeText}
                    value={this.state.alamat}
                    namaState="alamat"
                    isTextArea={true} />
                <TouchableOpacity
                    style={styles.tombol}
                    onPress={() => this.onSubmit()}
                >
                    <Text style={styles.textTombol}> Submit </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    pages: {
        flex: 1,
        padding: 30
    },
    tombol: {
        backgroundColor: 'black',
        padding: 10,
        borderRadius: 5,
        marginTop: 10
    },
    textTombol: {
        color: "#fff",
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 16
    }
})
