import TambahKontak from './TambahKontak'
import Home from './Home'
import DetailKontak from './DetailKontak'
import EditKontak from './EditKontak'


export {
    TambahKontak,
    Home,
    DetailKontak,
    EditKontak
}