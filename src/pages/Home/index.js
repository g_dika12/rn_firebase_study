import { faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Alert } from 'react-native'
import CardKontak from '../../components/CardKontak'
import FIREBASE from '../../config/FIREBASE'

export default class Home extends Component {
    constructor(props) {
        super(props)

        this.state = {
            kontak: {},
            kontakKey: []
        }
    }

    componentDidMount() {
        this.ambilData()
    }

    ambilData=()=>{
        FIREBASE.database()
            .ref("Kontak")
            .once("value", (querySnapShot) => {
                let data = querySnapShot.val() ? querySnapShot.val() : {};
                let kontakItem = { ...data }
                this.setState({
                    kontak: kontakItem,
                    kontakKey: Object.keys(kontakItem)
                })
            })
    }

    removeData = (id) => {
        Alert.alert(
            "Info",
            "Anda yakin akan menghapus data ini?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => {
                    FIREBASE.database()
                    .ref('Kontak/'+id)
                    .remove()

                    this.ambilData()
                    Alert.alert("Hapus", "Sukses Hapus Data")
                } }
            ],
            { cancelable: false }
        );
    }

    render() {
        const { kontak, kontakKey } = this.state;
        return (
            <View style={styles.page}>
                <View style={styles.header}>
                    <Text style={styles.title}>Daftar kontak</Text>
                    <View style={styles.garis} />
                </View>
                <View style={styles.listKontak}>
                    {kontakKey.length > 0 ? (
                        kontakKey.map((key) => (
                            <CardKontak
                                key={key}
                                kontakItem={kontak[key]}
                                id={key}
                                {...this.props}
                                removeData={this.removeData}
                            />
                        ))
                    ) : (
                        <Text>Kontak Kosong</Text>
                    )}
                </View>
                <View style={styles.wrapperButton}>
                    <TouchableOpacity
                        onPress={() => { this.props.navigation.navigate('TambahKontak') }}
                        style={styles.btnTambah}>
                        <FontAwesomeIcon icon={faPlus} color="white" />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    wrapperButton: {
        flex: 1,
        position: 'absolute',
        bottom: 0,
        right: 0,
        margin: 30
    },
    header: {
        paddingHorizontal: 30,
        paddingTop: 30
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    garis: {
        borderWidth: 1,
        marginTop: 10
    },
    listKontak: {
        paddingHorizontal: 30,
        marginTop: 20
    },
    btnTambah: {
        padding: 20,
        backgroundColor: 'skyblue',
        borderRadius: 30,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,
    }
})
